# Perception-projet

Ce repository contient le matériel nécessaire à réaliser le Projet de l'UE Perception pour la robotique.
Le projet sera effectué sur le robot mobile e-puck, il sera donc nécessaire de récuperer aussi le projet epuck-client:

[https://gitlab.com/umrob/epuck-client](https://gitlab.com/umrob/epuck-client)

Notamment songez à lire le mode d'emploi du robot : 

[mode_emploi_epuck_etudiants.pdf](https://gitlab.com/umrob/epuck-client/blob/master/mode_emploi_epuck_etudiants.pdf)

dans epuck-client.

Le projet est constitué de quatre parties:

1) Etalonnage
2) Poursuite de cible
3) Localisation par fusion de données
4) Asservissement visuel

Bon travail!
